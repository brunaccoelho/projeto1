# pacote1

> Pacote de exemplo

## Instalação

> Requer PHP 7 e a extensão 'mbstring'
```bash
composer require brunaccoelho/pacote1
```
# API
```
namespace brunaccoelho\pacote1;
class Exemplo {
   //Retorna o nome 
    function nome();
}
```

## Licença 

MIT